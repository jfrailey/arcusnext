// Copyright www.Hupla.com :-)
// All Rights Reserved

// Package service package will document all available endpoints and the parameters available
// This is a good place to assure the URL is properly formed.
// Production system would probably evolve to annotations and/or
// generated code coming from something like
//
//   swagger
//   raml
//
// and utilize the ability to render html in browser with added features like "try it now!"
package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"hupla.com/darksky"

	"google.golang.org/appengine"
)

const (
	weatherParamsSize = 3
)

const (
	w_latitude = iota
	w_longitude
	w_time
)

// Weather endpoint for darksky API service
// URL format: <host>:port/v1/<darksky.net developer key>/<float latitude>,<float longitude>,<RFC3339 date or unix epoch in seconds>
//   params:
//   key - license key for darksky
//   variables - comma separated list to match the darksky API
//       latitude (float64)
//       longitued (float64)
//       startDate (int64 for epoch OR string RFC3339)
func Weather(w http.ResponseWriter, r *http.Request) {

	//get all params out of the URL
	params := mux.Vars(r)
	key := params["key"]
	variables := params["variables"]
	weatherParams := strings.Split(variables, ",")

	//QueryString params
	u, _ := url.Parse(r.URL.String())
	lang := u.Query().Get("lang")
	units := u.Query().Get("units")

	if len(weatherParams) != weatherParamsSize {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Invalid number of parameters! (3) required [lat, long, epoch-time"))
		return
	}

	weatherRequest, err := validateWeatherParams(&weatherParams)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad Parameters! " + err.Error()))
		return
	}
	weatherRequest.ApiKey = key
	if len(lang) > 0 {
		weatherRequest.Language = darksky.Lang(lang)
	}
	if len(units) > 0 {
		weatherRequest.Units = darksky.Units(units)
	}

	ctx := appengine.NewContext(r)
	wd, error := darksky.GetWeatherHistory(ctx, weatherRequest)
	if error != nil {
		w.WriteHeader(http.StatusBadGateway)
		w.Write([]byte("502 - Rejected API call! " + error.Error()))
		return
	}

	//made it this far, must be weather data !! congrats!!
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(wd)

}

// Info - default endpoint to get generic version and description of microService
// it is also the default redirect if anyone lands on "/"
func Info(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Weather History microService V1.0\nAuthor: John Frailey\nemail: john@hupla.com, dfense@gmail.com\ndate: 11/04/2017"))
}

// RootHandler - default "/" endpoint handler
func RootHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/v1/info", http.StatusFound)
}

// HealthCheck - placeholder to put instrumention in.
//   statsD
//   promethius
//   influxData telegraph etc
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

// validateWeatherParams - helper function to pre-validate param formats
// are valid before contacting darksky API. This will cut down on the bad requests.
func validateWeatherParams(params *[]string) (darksky.WeatherRequest, error) {

	errorStrings := make([]string, 0)
	wr := darksky.NewWeatherRequestDefault()

	// make sure latitude is a good float
	lat, err := strconv.ParseFloat((*params)[w_latitude], 64)
	if err != nil {
		errorStrings = append(errorStrings, "Latitude is not a valid float")
	} else {
		wr.Latitude = darksky.Measurement(lat)
	}
	// make sure longitude is a good float
	long, err := strconv.ParseFloat((*params)[w_longitude], 64)
	if err != nil {
		errorStrings = append(errorStrings, "Longitude is not a valid float")
	} else {
		wr.Longitude = darksky.Measurement(long)
	}

	//see if date needs converting
	ts, err := strconv.ParseInt((*params)[w_time], 10, 64)
	if err != nil {
		//if it's not convertable to an int, might be an RFC3339
		// Parse consistent time so can make good tests
		startDate, e := time.Parse(
			time.RFC3339,
			(*params)[w_time])
		//"2017-01-01T12:00:00.000-06:00") //CST time zone offset
		if e != nil {
			errorStrings = append(errorStrings, "Invalid Date Parameter")
		} else {
			ts = startDate.Unix()
		}
	}
	wr.EpochTime = darksky.Timestamp(ts)

	// if errors, concatenate them together and build an error struct
	if len(errorStrings) != 0 { //errors accumulated

		var buffer bytes.Buffer
		for i, e := range errorStrings {
			if i > 0 {
				buffer.WriteString(":")
			}
			buffer.WriteString(e)

		}
		return wr, errors.New(buffer.String())
	}
	return wr, nil
}
