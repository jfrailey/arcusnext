package darksky

import (
	"encoding/json"
	"io"
)

// Timestamp is an int64 timestamp
type Timestamp int64

// Measurement is a float64 measurement
type Measurement float64

// Flags of all meta-data from weather data
type Flags struct {
	DarkSkyUnavailable string   `json:"darksky-unavailable,omitempty"`
	DarkSkyStations    []string `json:"darksky-stations,omitempty"`
	DataPointStations  []string `json:"datapoint-stations,omitempty"`
	ISDStations        []string `json:"isd-stations,omitempty"`
	LAMPStations       []string `json:"lamp-stations,omitempty"`
	MADISStations      []string `json:"madis-stations,omitempty"`
	METARStations      []string `json:"metars-stations,omitempty"`
	METNOLicense       string   `json:"metnol-license,omitempty"`
	Sources            []string `json:"sources,omitempty"`
	Units              string   `json:"units,omitempty"`
}

// DataPoint contains various properties, each representing the average (unless otherwise specified) of a particular weather phenomenon occurring during a period of time.
type DataPoint struct {
	ApparentTemperature        Measurement `json:"apparentTemperature,omitempty"`
	ApparentTemperatureMax     Measurement `json:"apparentTemperatureMax,omitempty"`
	ApparentTemperatureMaxTime Timestamp   `json:"apparentTemperatureMaxTime,omitempty"`
	ApparentTemperatureMin     Measurement `json:"apparentTemperatureMin,omitempty"`
	ApparentTemperatureMinTime Timestamp   `json:"apparentTemperatureMinTime,omitempty"`
	CloudCover                 Measurement `json:"cloudCover,omitempty"`
	DewPoint                   Measurement `json:"dewPoint,omitempty"`
	Humidity                   Measurement `json:"humidity,omitempty"`
	Icon                       string      `json:"icon,omitempty"`
	MoonPhase                  Measurement `json:"moonPhase,omitempty"`
	NearestStormBearing        Measurement `json:"nearestStormBearing,omitempty"`
	NearestStormDistance       Measurement `json:"nearestStormDistance,omitempty"`
	Ozone                      Measurement `json:"ozone,omitempty"`
	PrecipAccumulation         Measurement `json:"precipAccumulation,omitempty"`
	PrecipIntensity            Measurement `json:"precipIntensity,omitempty"`
	PrecipIntensityMax         Measurement `json:"precipIntensityMax,omitempty"`
	PrecipIntensityMaxTime     Timestamp   `json:"precipIntensityMaxTime,omitempty"`
	PrecipProbability          Measurement `json:"precipProbability,omitempty"`
	PrecipType                 string      `json:"precipType,omitempty"`
	Pressure                   Measurement `json:"pressure,omitempty"`
	Summary                    string      `json:"summary,omitempty"`
	SunriseTime                Timestamp   `json:"sunriseTime,omitempty"`
	SunsetTime                 Timestamp   `json:"sunsetTime,omitempty"`
	Temperature                Measurement `json:"temperature,omitempty"`
	TemperatureHigh            Measurement `json:"temperatureHigh,omitempty"`
	TemperatureLow             Measurement `json:"temperatureLow, omitempty"`
	TemperatureLowTime         Timestamp   `json:"temperatureLowTime, omitempty"`
	TemperatureMax             Measurement `json:"temperatureMax,omitempty"`
	TemperatureMaxTime         Timestamp   `json:"temperatureMaxTime,omitempty"`
	TemperatureMin             Measurement `json:"temperatureMin,omitempty"`
	TemperatureMinTime         Timestamp   `json:"temperatureMinTime,omitempty"`
	Time                       Timestamp   `json:"time,omitempty"`
	UvIndex                    int64       `json:"uvIndex,omitempty"`
	UvIndexTime                Timestamp   `json:"uvIndexTime,omitempty"`
	Visibility                 Measurement `json:"visibility,omitempty"`
	WindBearing                Measurement `json:"windBearing,omitempty"`
	WindGust                   Measurement `json:"windGust,omitempty"`
	WindGustTime               Timestamp   `json:"windGustTime,omitempty"`
	WindSpeed                  Measurement `json:"windSpeed,omitempty"`
}

//DataBlock is the heart and detail of the sensor weather data. We will collect one of these for each day we request
type DataBlock struct {
	Summary string      `json:"summary,omitempty"`
	Icon    string      `json:"icon,omitempty"`
	Data    []DataPoint `json:"data"`
}

type Forecast struct {
	Latitude  float64    `json:"latitude,omitempty"`
	Longitude float64    `json:"longitude,omitempty"`
	Timezone  string     `json:"timezone,omitempty"`
	Offset    float64    `json:"offset,omitempty"`
	Currently DataPoint  `json:"-"`
	Minutely  DataBlock  `json:"-"`
	Hourly    DataBlock  `json:"-"`
	Daily     *DataBlock `json:"daily,omitempty"`
	Flags     Flags      `json:"flags,omitempty"`
	APICalls  int        `json:"apicalls,omitempty"`
	Code      int        `json:"code,omitempty"`
}

// ForecastHistory - by composition embeds a Forecast inside struct, and adds a slice of DataBlocks
// this will keep from having redundant struct definitions that are so much alike
type ForecastHistory struct {
	Forecast  Forecast     `json:"weatherTimeMachineReport,omitempty"`
	DailyList []*DataBlock `json:"daily,omitempty"`
}

type Units string

const (
	CA   Units = "ca"
	SI   Units = "si"
	US   Units = "us"
	UK   Units = "uk"
	AUTO Units = "auto"
)

type Lang string

const (
	Arabic             Lang = "ar"
	Azerbaijani        Lang = "az"
	Belarusian         Lang = "be"
	Bosnian            Lang = "bs"
	Catalan            Lang = "ca"
	Czech              Lang = "cs"
	German             Lang = "de"
	Greek              Lang = "el"
	English            Lang = "en"
	Spanish            Lang = "es"
	Estonian           Lang = "et"
	French             Lang = "fr"
	Croatian           Lang = "hr"
	Hungarian          Lang = "hu"
	Indonesian         Lang = "id"
	Italian            Lang = "it"
	Icelandic          Lang = "is"
	Cornish            Lang = "kw"
	Indonesia          Lang = "nb"
	Dutch              Lang = "nl"
	Polish             Lang = "pl"
	Portuguese         Lang = "pt"
	Russian            Lang = "ru"
	Slovak             Lang = "sk"
	Slovenian          Lang = "sl"
	Serbian            Lang = "sr"
	Swedish            Lang = "sv"
	Tetum              Lang = "te"
	Turkish            Lang = "tr"
	Ukrainian          Lang = "uk"
	IgpayAtinlay       Lang = "x-pig-latin"
	SimplifiedChinese  Lang = "zh"
	TraditionalChinese Lang = "zh-tw"
)

// FromJSON reads from a reader into the Forecast struct
func FromJSON(reader io.Reader) (*Forecast, error) {
	var f Forecast
	if err := json.NewDecoder(reader).Decode(&f); err != nil {
		return nil, err
	}

	return &f, nil
}
