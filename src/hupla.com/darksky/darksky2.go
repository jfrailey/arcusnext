// Package darksky encapsulates all model and network calls to the darksky API service.
// It implements compliance with Google Cloud Platform and utilizes the fetchURL compliance
// to do all the exchanges over the network with darksky API
package darksky

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"google.golang.org/appengine/urlfetch"
)

const (
	//secondsInDay used to roll back (1) day at a time.
	secondsInDay = 86400
	//maxDaysToRetrieve allows some guardrails on max days
	maxDaysToRetrieve = 10
	//baseURL is prefix of darksky API URL
	baseURL = "https://api.darksky.net/forecast/"
	// URL example:  "https://api.darksky.net/forecast/APIKEY/LATITUDE,LONGITUDE,TIME?units=ca&lang=en&exclude=currently,minutely,hourly"
)

var daysToRetrieve = 7

type WeatherRequest struct {
	Latitude  Measurement
	Longitude Measurement
	EpochTime Timestamp
	ApiKey    string
	Language  Lang
	Units     Units
}

// NewWeatherRequest - created a new WeatherRequest filled in with a
//   Default Location <1200 E. 151st St. Olathe, Ks. 66062-3426>,
//   EpochTime,
//   Language,
//   Units NOTE:
//   ApiKey NOT filled
// Can be used as is for test (fill in ApiKey) or override
// appropriate fields
func NewWeatherRequestDefault() WeatherRequest {

	lat := Measurement(38.856147)
	long := Measurement(-94.800953)
	epochNow := Timestamp(time.Now().Unix())

	return WeatherRequest{Latitude: lat, Longitude: long, EpochTime: epochNow, Language: English, Units: US}
}

// GetWeatherHistory - convenient function to call the darksky API. The darksky API
// will only return (1)  Time Machine Request  at a time. This function will make (n) calls
// based on the <b>daysToRetrieve</b> const defined in the top of this file.
//
// Usage:
// This function will take in a valid
//   WeatherRequest
// and return a JSON []byte array that can be written by any REST endpoint.
func GetWeatherHistory(ctx context.Context, wr WeatherRequest) (ForecastHistory, error) {

	var fcHistory ForecastHistory
	var fc *Forecast
	var err error
	var lastEpoch = int64(wr.EpochTime)

	//cycle through all the daysToRetrieve decrementing 24hrs at a time
	for i := 0; i < daysToRetrieve; i++ {

		fc, err = fetchWeatherGCS(ctx, wr)
		//round off the day,and put as DailySummary
		if err != nil {
			return fcHistory, err
		}

		//first pass fills in entire struct, remainder only DailyList
		if i == 0 {
			fcHistory.DailyList = make([]*DataBlock, 0)
			fcHistory.Forecast = *fc
			fcHistory.DailyList = append(fcHistory.DailyList, fc.Daily)
		} else { //otherwise just add the Daily datablock
			fcHistory.DailyList = append(fcHistory.DailyList, fc.Daily)
		}
		fcHistory.Forecast.Daily = nil
		lastEpoch -= secondsInDay

		// possible came back with no data for those coord or time
		if fc.Daily == nil {
			db := new(DataBlock)
			db.Summary = time.Unix(lastEpoch, 0).Format("2006-01-02") + " - no data found"
			fcHistory.DailyList[i] = db
		} else {
			fc.Daily.Summary = time.Unix(lastEpoch, 0).Format("2006-01-02")
		}

		wr.EpochTime = Timestamp(lastEpoch)
	}
	return fcHistory, err
}

//FetchWeather is isolated to retrieving the network API data direct from darksky.
//This function uses a fully compliant Google Cloud Service - fetchURL approach.
//Alternate Fetches can be developed for NON-GCS usage.
func fetchWeatherGCS(ctx context.Context, wr WeatherRequest) (*Forecast, error) {

	// convert native types back to string
	lat := strconv.FormatFloat(float64(wr.Latitude), 'f', 6, 64)
	long := strconv.FormatFloat(float64(wr.Longitude), 'f', 6, 64)
	epoch := strconv.FormatInt(int64(wr.EpochTime), 10)

	//build URL - lot can be improved here, on options like put exclude params in a map and default unit settings be overridden
	url := baseURL + wr.ApiKey + "/" + lat + "," + long + "," + epoch + "?units=" + string(wr.Units) +
		"&lang=" + string(wr.Language) + "&exclude=currently,minutely,hourly" //parmas + options

	//	log.Println("URL:" + url)
	client := urlfetch.Client(ctx)
	resp, err := client.Get(url)

	if err != nil || resp.StatusCode != http.StatusOK {
		return nil, errors.New("Bad HTTP Response Code from API: " + resp.Status)
	}
	defer resp.Body.Close()

	f, err := FromJSON(resp.Body)
	if err != nil {
		return nil, err
	}
	return f, nil
}
