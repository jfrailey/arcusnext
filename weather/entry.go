// Package service Entry file for init() on Google Cloud Service
// We map all of our Endpoint URL's to func
package service

import (
	"net/http"

	"github.com/gorilla/mux"
	"hupla.com/service"
)

// Entry Point for GCS and Startup to Serve a microService to retrieve Weather info
// based upon a latitude/longitude location, and the date to retrieve + back (7) days <configurable>
func init() {
	rtr := mux.NewRouter()
	rtr.StrictSlash(true)
	// Root URL
	rtr.HandleFunc("/", service.RootHandler).Methods("GET")
	// /info - provides general info (could evolve to diag and live metrics)
	rtr.HandleFunc("/v1/info", service.Info).Methods("GET")
	// /weather - this is the main weather API service call
	rtr.HandleFunc("/v1/{key}/{variables}", service.Weather).Methods("GET")

	// bind gorilla mux to http server
	http.Handle("/", rtr)
}
