# Arcusnext backend developer test application

I'd like to thank Arcusnext for letting me have the opportunity to participate in your screening program. I like any excuse to write more GO code :-)

## Install and Build project
I originally was using the GoogleCloudPlatform repository as my vcs, but believe it may be a challenge trying to get all the Arcusnext accounts if they are non-google, access to the repo. I am hoping everyone has access to bitbucket, and i get to use this cool markdown for document.

#### Assumptions
Everyone has access to the bitbucket account to check out the project
The reviewers will have GCS SDK installed, so instructions assume that step is well know. I will focus more on making sure the checkout occurs, and dependencies resolved.

#### Install instructions
instructions below will use a $ as an indicator the prompt is present, and you will type out the commands.


```
#!golang
//make any directory and cd to it. Referred from here on as project
$ cd project
$ git clone https://jfrailey@bitbucket.org/jfrailey/arcusnext.git
$ cd arcusnext
$ export GOPATH=$PWD
$ cd src
$ go get ./...
$ cd ../weather
$ gcloud init
$ dev_appserver.py app.yaml 
```
All goes well, you will get some feedback that everything is running like this:

```
#!golang
dfense@dfenseVB /tmp/a2/arcusnext/weather $ dev_appserver.py app.yaml 
INFO     2017-11-06 01:52:10,496 devappserver2.py:105] Skipping SDK update check.
INFO     2017-11-06 01:52:10,782 api_server.py:300] Starting API server at: http://localhost:35873
INFO     2017-11-06 01:52:10,806 dispatcher.py:251] Starting module "default" running at: http://localhost:8080
INFO     2017-11-06 01:52:10,807 admin_server.py:116] Starting admin server at: http://localhost:8000
```

Go ahead and try hit it with local browswer:

http://localhost:8080/v1/1bbcba2a301aa7064fd38b0c5294368d/37.82678,-122.4233,2017-12-31T12:00:00-06:00?lang=fr&units=ca

# Usage and Features #
I tried to model the darksky URL since it was well thought out, and easy to understand. With that stated, here is the layout, and options on the URL usage.

production deployed URL: https://arcusnext-184701.appspot.com/v1/1bbcba2a301aa7064fd38b0c5294368d/38.856147,-94.800953,2017-12-31T12:00:00-06:00?lang=fr&units=us

```
#!golang
http://arcu...com/v1/1bbcba2a301aa7064fd38b0c5294368d/38.856147,-94.800953,2017-12-31T12:00:00-06:00?lang=fr&units=ca
\--- my host ---/\v/\------------ dev token --------/\-lat--/ \- long -/\-- RFC3339 or epoch  ------/ \lan-/  \-units-/
```
### Notes ### 
* Any of the que* ry parameters (after the ?) are optional. Defaults are lang=en, units=us  
* Can use epoch time or RFC3339 format time  
* If you use from a browser, best viewed if you have a JSON plugin for rendering JSON in a tree. 

### godoc ###
Might be easier reviewing the signatures by a quick startup of go docs and review the API with your browser. I tried documenting it where it made sense to view from here


```
#!golang
//assuming your GOPATH is still set
$ godoc -http=:6060
```
hit this URL to go to the correct package: http://localhost:6060/pkg/hupla.com/

## Level Of Effort
It took me much longer than it should have. I soaked time in (2) areas

* I have not deployed apps to GCS before, even though i am porting a large complex container appliance on bare metal today, to AWS, i had to learn some GCS specific quirks. I had originally wrote it like a standalone http server, and had to refactor once i understood running the GCS server. All that code is in my weather/entry.go code.
* I had written a much more brief library around the darksky API go library. This did not work in GCS, so decided to extract some structs and ideas from darksky lib, and write my own using the fetchUrl approach

My breakdown looks like this

Day      | Approx time  | Description
-------- | -------------| -----------
Thursday | 2.5hrs       | Went through quick start and got enough to bootstrap up a skeleton deploy project
Saturday | 3.5hrs       | Write UNIT test and a darksky API library to abstract anything but request params
Sunday   | 6.5-7.5hrs   | Finished endpoint on standalone server, to find out it did not work, and spent rest of time refactoring for not only the darksky lib using fetchUrl , but also the endpoint mapping and init. 

# Areas I wanted to improve #
* UNIT testing. I'm usually quite religous. I had good ones until refactor, then I saw a different approach is needed, and not enough time to learn the GCS way. Anxious to do so. 
* Optimizing, and refactor any common code like the HTTP errors, and writing to ResponseWriter etc.
* Learning more about GCS. I think I can apply a lot of what i have learned using AWS to GCS

# Summary #
Thank Arcusnext, Matt for finding me, and all the reviewers, as I am no stranger to the level of effort that goes into the review process. I hired nearly 25 programmers while at garmin in my group, and nearly 12 here at www.cree.com 

If you decide to continue the process after your review, I can bring in quite a bit of Architectural designs i have done with GO that include networking of all sorts, binary encoders for low level networks, microService transport layer protocols using GPB and gRPC, and lots of diagrams to chat about how i designed Containers and message queues, and of course SECURITY. TLS1.2 with certs, JWT token signing, secure-boot for an MX6 processor i did a Yocto boot for on a wireless mesh gateway, AES256 for light network, REST API and Swagger docs, SDK for client code, and more...

Cheers !!